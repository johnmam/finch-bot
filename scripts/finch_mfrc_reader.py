#!/usr/bin/python
import json
import rospy
import MFRC522
import signal
import os
from finch_bot.msg import finch_mfrc522

import smbus
from time import sleep
import time
import rospy
from finch_bot.msg import finch_color

continue_reading  = True
def end_read(signal,frame):
    print "Ending Read"
    continue_reading = False

class finch_mfrc_reader(object):

    def mfrc522_read(self):
        rate = rospy.Rate(10) # 10hz
        while continue_reading and not rospy.is_shutdown():

            self.counter = self.counter + 1;

            if(self.counter % 5 == 0) :
                # Scan for cards
                (status,TagType) = self.mifareReader.MFRC522_Request(self.mifareReader.PICC_REQIDL)
                if status == self.mifareReader.MI_OK:
                    print "Card detected"
                # Get the UID of the card
                    (status,uid) = self.mifareReader.MFRC522_Anticoll()
                    if status == self.mifareReader.MI_OK:
                        #print "Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
                        strID = str(uid[0])+str(uid[1])+str(uid[2])+str(uid[3])
                        msg = finch_mfrc522()
                        msg.utime = rospy.Time.now()
                        if strID in self.Mifare:
                            msg.uid = self.Mifare[strID]
                            msg.long_id = strID
                            rospy.loginfo(msg)
                            self.pub.publish(msg)
                        else:
                            msg.uid = -1
                            msg.long_id = strID
                            print "-----------detect card, but id not in table------------"
                            rospy.loginfo(msg)
                            self.pub.publish(msg)
                    else:
                        msg = finch_mfrc522()
                        msg.utime = rospy.Time.now()
                        msg.uid = -1
                        rospy.loginfo(msg)
                        self.pub.publish(msg)


            idx = 1
            for idx in range(0,2):
                t1 = time.time()
                self.bus.write_byte(0x70,1 << idx)
                data = self.bus.read_i2c_block_data(0x29, 0)
                clear = float(data[1] << 8 | data[0])
                red   = float(data[3] << 8 | data[2])
                green = float(data[5] << 8 | data[4])
                blue  = float(data[7] << 8 | data[6])

                fc = finch_color();
                fc.id = idx;
                fc.rgbc[0] = red;
                fc.rgbc[1] = green;
                fc.rgbc[2] = blue;
                fc.rgbc[3] = clear;

                t2 = time.time()

                self.pub_color.publish(fc)

            rate.sleep()

    def __init__(self):

        self.counter = 0;

        with open(os.path.dirname(__file__) + '/rfid_dict') as data_file:
            self.Mifare = json.load(data_file)
        print(self.Mifare);

        self.mifareReader = MFRC522.MFRC522();
        rospy.init_node('mfrc522', anonymous=True)
        self.pub = rospy.Publisher('finch_mfrc', finch_mfrc522, queue_size=10)

        self.bus = smbus.SMBus(1)
        self.pub_color = rospy.Publisher('finch_color', finch_color, queue_size=10)

        ok = True;
        idx = 1

        self.fcs = []
        for idx in range(0,2):
            self.bus.write_byte(0x70,1 << idx)
            new_idx = self.bus.read_byte(0x70)

            print "Sent " + str(idx) + " got " + str(new_idx) + "\n"

            # I2C address 0x29
            # Register 0x12 has device ver.
            # Register addresses must be OR'ed with 0x80
            self.bus.write_byte(0x29,0x80|0x12)
            ver = self.bus.read_byte(0x29)
            # version # should be 0x44
            if ver == 0x44:
                self.bus.write_byte_data(0x29, 0x80|0x00, 0x01) # 0x00 = ENABLE register, 0x01 power on
                self.bus.write_byte_data(0x29, 0x80|0x00, 0x01|0x02) # 0x01 = Power on, 0x02 RGB sensors enabled

                self.bus.write_byte_data(0x29,0x80|0x01, 0xff)
                atime = self.bus.read_byte(0x29)

                self.bus.write_byte_data(0x29,0x80|0x03, 0xff)
                wtime = self.bus.read_byte(0x29)

                self.bus.write_byte(0x29, 0x80|0x14) # Reading results start register 14, LSB then MSB
                print "Device " + str(idx) + " found atime: %d wtime %d\n" % (atime, wtime)

            else:
                print "Device " + str(idx) + " not found " + str(ver) +" instead\n"
                ok = False

        if ok == True:
            self.mfrc522_read();

if __name__ == '__main__':
    try:
        signal.signal(signal.SIGINT,end_read)
        finch_m_reader = finch_mfrc_reader()
    except rospy.ROSInterruptException:
        pass
