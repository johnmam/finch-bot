#!/usr/bin/env python
import rospy
import sys
from finch_bot.msg import traffic_light_cmd

# Manually control traffic light colors:
# Usage: traffic_light_cmd_spoofer light_id r_value g_value b_value [0-255]

def traffic_light_cmd_spoofer():
    if(len(sys.argv) < 5):
        print("Usage: traffic_light_cmd_spoofer light_id r_value g_value b_value [0-255]")
        return

    pub = rospy.Publisher('traffic_light_cmd', traffic_light_cmd, queue_size=10)
    rospy.init_node('traffic_light_cmd_spoofer', anonymous=True)

    tl_cmd = traffic_light_cmd()

    tl_cmd.numLights = int(sys.argv[1])

    for idx, val in enumerate(sys.argv):
        if (idx > 1):
            tl_cmd.rgb_array.append(int(sys.argv[idx]))

    print(tl_cmd)
    pub.publish(tl_cmd)

if __name__ == '__main__':
    try:
        traffic_light_cmd_spoofer()
    except rospy.ROSInterruptException:
        pass
