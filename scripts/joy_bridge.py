#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_motor_cmd
from finch_bot.msg import finch_aux_cmd

class joy_bridge(object):

    def callback(self, data):
        #rospy.loginfo(rospy.get_caller_id() + 'I heard:')
        #for axe in data.axes:
            #rospy.loginfo(rospy.get_caller_id() + '    ' + str(axe) )
        #for but in data.buttons:
            #rospy.loginfo(rospy.get_caller_id() + '    ' + str(but) )

        mc = finch_motor_cmd();

        mc.halt = 0;
        mc.left = data.axes[1];
        mc.right = data.axes[4];

        #rospy.loginfo(mc)
        self.pub.publish(mc)

        if any(data.buttons) :

            if(data.buttons[0]):
                self.ac.led = [255, 0, 0];
            if(data.buttons[1]):
                self.ac.led = [ 0, 255, 0];
            if(data.buttons[2]):
                self.ac.led = [ 0, 0, 255];
            if(data.buttons[3]):
                self.ac.led = [ 255, 255, 255];
            if(data.buttons[4]):
                self.ac.led = [ 0, 255, 255];
            if(data.buttons[5]):
                self.ac.led = [ 255, 0, 255];
            if(data.buttons[6]):
                self.ac.led = [ 255, 255, 0];
            if(data.buttons[7]):
                self.ac.led = [ 0, 0, 0];

            self.pub_aux.publish(self.ac)


    def __init__(self, *args, **kwds):
        rospy.init_node('joy_bridge', anonymous=True)

        self.ac = finch_aux_cmd();

        self.pub = rospy.Publisher('finch_motor_cmd', finch_motor_cmd, queue_size=10)
        self.pub_aux = rospy.Publisher('finch_aux_cmd', finch_aux_cmd, queue_size=10)

        rospy.Subscriber('joy', Joy, self.callback)

        rospy.spin()

if __name__ == '__main__':
    jb = joy_bridge()
