#!/usr/bin/env python
import smbus
from time import sleep
import time
import rospy
from finch_bot.msg import finch_color

class color_driver(object):

    def get_color(self):
	rate = rospy.Rate(20) # 10hz
        while not rospy.is_shutdown():
            idx = 1
            for idx in range(0,2):
                t1 = time.time()
                self.bus.write_byte(0x70,1 << idx)
                data = self.bus.read_i2c_block_data(0x29, 0)
                clear = float(data[1] << 8 | data[0])
                red   = float(data[3] << 8 | data[2])
                green = float(data[5] << 8 | data[4])
                blue  = float(data[7] << 8 | data[6])

                fc = finch_color();
                fc.id = idx;
                fc.rgbc[0] = red;
                fc.rgbc[1] = green;
                fc.rgbc[2] = blue;
                fc.rgbc[3] = clear;

                t2 = time.time()

                self.pub.publish(fc)

            rate.sleep()
        print "Done in ROS main\n"


    def __init__(self, *args, **kwds):
        self.bus = smbus.SMBus(1)

        rospy.init_node('color_true', anonymous=True)
        self.pub = rospy.Publisher('finch_color', finch_color, queue_size=10)

        ok = True;
        idx = 1

        self.fcs = []
        for idx in range(0,2):
            self.bus.write_byte(0x70,1 << idx)
            new_idx = self.bus.read_byte(0x70)

            print "Sent " + str(idx) + " got " + str(new_idx) + "\n"

            # I2C address 0x29
            # Register 0x12 has device ver.
            # Register addresses must be OR'ed with 0x80
            self.bus.write_byte(0x29,0x80|0x12)
            ver = self.bus.read_byte(0x29)
            # version # should be 0x44
            if ver == 0x44:
                self.bus.write_byte_data(0x29, 0x80|0x00, 0x01) # 0x00 = ENABLE register, 0x01 power on
                self.bus.write_byte_data(0x29, 0x80|0x00, 0x01|0x02) # 0x01 = Power on, 0x02 RGB sensors enabled

                self.bus.write_byte_data(0x29,0x80|0x01, 0xff)
                atime = self.bus.read_byte(0x29)

                self.bus.write_byte_data(0x29,0x80|0x03, 0xff)
                wtime = self.bus.read_byte(0x29)

                self.bus.write_byte(0x29, 0x80|0x14) # Reading results start register 14, LSB then MSB
                print "Device " + str(idx) + " found atime: %d wtime %d\n" % (atime, wtime)

            else:
                print "Device " + str(idx) + " not found " + str(ver) +" instead\n"
                ok = False

        if ok == True:
            self.get_color();


    def __del__(self):
        pass

if __name__ == '__main__':
    fd = color_driver();
