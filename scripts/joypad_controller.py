#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import Joy
from finch_bot.msg import finch_joy_cmd

ID_ALL = 255;

BTN_RED   = 1;
BTN_GREEN = 0;
BTN_BLUE  = 2;
BTN_YELLOW  = 3;
LB = 4;
RB = 5;
START = 7;

ROBOT_NAMES = ["","finch1", "finch2", "finch3", "finch4"];

class joypad_controller(object):


    def publish(self, mode):
        fc = finch_joy_cmd();
        fc.mode = mode;
        fc.turn_mode = self.turn_mode;
        fc.robot = self.robot_id;
        self.pub.publish(fc);

    def joy_cb(self, msg):

        if(msg.buttons[BTN_RED]):
            self.robot_id = BTN_RED + 1;
            rospy.loginfo("ROBOT %s SELECTED", ROBOT_NAMES[self.robot_id]);
        elif(msg.buttons[BTN_GREEN]):
            self.robot_id = BTN_GREEN + 1;
            rospy.loginfo("ROBOT %s SELECTED", ROBOT_NAMES[self.robot_id]);
        elif(msg.buttons[BTN_BLUE]):
            self.robot_id = BTN_BLUE + 1;
            rospy.loginfo("ROBOT %s SELECTED", ROBOT_NAMES[self.robot_id]);
        elif(msg.buttons[BTN_YELLOW]):
            self.robot_id = BTN_YELLOW + 1;
            rospy.loginfo("ROBOT %s SELECTED", ROBOT_NAMES[self.robot_id]);
        elif(msg.buttons[START]):
            self.robot_id = finch_joy_cmd.ALL_ROBOTS;
            rospy.loginfo("ALL ROBOTS SELECTED");

        if(msg.buttons[RB]):
            self.publish(finch_joy_cmd.MODE_STOP);
            rospy.loginfo("STOPPING");
        elif(msg.buttons[LB]):
            self.publish(finch_joy_cmd.MODE_LINE);
            rospy.loginfo("GOING");
        elif(msg.axes[6] < -0.5):
            self.turn_mode = finch_joy_cmd.RIGHT_TURN;
            self.publish(finch_joy_cmd.MODE_DEAD);
            rospy.loginfo("RIGHT");
        elif(msg.axes[6] > 0.5):
            self.turn_mode = finch_joy_cmd.LEFT_TURN;
            self.publish(finch_joy_cmd.MODE_DEAD);
            rospy.loginfo("LEFT");
        elif(msg.axes[7] > 0.5):
            self.turn_mode = finch_joy_cmd.NO_TURN;
            self.publish(finch_joy_cmd.MODE_DEAD);
            rospy.loginfo("FORWARD");




    def __init__(self, *args, **kwds):

        self.robot_id = finch_joy_cmd.ALL_ROBOTS;
        self.turn_mode = finch_joy_cmd.NO_TURN;

        rospy.init_node('joypad_controller', anonymous=True)

        self.pub = rospy.Publisher('finch_joy_cmd', finch_joy_cmd, queue_size=10)
        rospy.Subscriber('joy', Joy, self.joy_cb)

        rospy.spin()

    def __del__(self):
        pass

if __name__ == '__main__':
    fd = joypad_controller();
